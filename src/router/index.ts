import { createRouter, createWebHashHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import LogView from '../views/LogView.vue'
import ExportView from '../views/ExportView.vue'

const router = createRouter({
  history: createWebHashHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/log',
      name: 'log',
      component: LogView
    },
    {
      path: '/export',
      name: 'export',
      component: ExportView
    }
  ]
})

export default router
