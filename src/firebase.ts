// Import the functions you need from the SDKs you need
import { initializeApp } from 'firebase/app'

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyCLSA3usMX2nq9JTQfSUAmNzwlOwL545OI",
  authDomain: "weight-journey-f82b5.firebaseapp.com",
  projectId: "weight-journey-f82b5",
  storageBucket: "weight-journey-f82b5.appspot.com",
  messagingSenderId: "708156661406",
  appId: "1:708156661406:web:fe848144417b3632e6a58f",
  // The value of `databaseURL` depends on the location of the database
  databaseURL: "https://weight-journey-f82b5-default-rtdb.firebaseio.com",
}

// Initialize Firebase
initializeApp(firebaseConfig);
