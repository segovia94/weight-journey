import { defineStore } from 'pinia'
import { getDatabase, ref, set, onValue, remove } from 'firebase/database'

const db = getDatabase();

interface Log {
  id: number;
  date: string;
  weight: number;
}

export const useWeightStore = defineStore({
  id: 'weightData',

  state: () => ({
    data: [] as Log[]
  }),

  getters: {
    sorted: (state) => {
      const data = state.data.slice()
      return data.sort((a: Log, b: Log) => (a.date > b.date) ? 1 : -1)
    },

    reverse() { return this.sorted.slice().reverse() },

    oldestDate() { return this.sorted[0]?.date },

    oldestWeight() { return this.sorted[0]?.weight },

    newestDate() { return this.reverse[0]?.date },

    newestWeight() { return this.reverse[0]?.weight },
  },

  actions: {
    load() {
      const logs = ref(db, '/logs')
      onValue(logs, (snapshot) => {
        this.data = Object.values(snapshot.val() ?? {})
      });
    },

    add(date: string, weight: number) {
      const id = Date.now()
      const log: Log = {
        id,
        'date': date,
        'weight': weight
      }

      set(ref(db, `logs/${id}`), log)
    },

    remove(id: number) {
      remove(ref(db, `logs/${id}`))
    },

    import(data: string) {
      // Validate that this is in the correct format
      let dataObj = {}
      try {
        dataObj = JSON.parse(data)
      } catch (error) {
        return false
      }

      if (typeof dataObj !== 'object' || dataObj === null) {
        return false
      }

      const firstKey = Object.keys(dataObj)[0]
      if (!dataObj[firstKey].hasOwnProperty('id')
        || !dataObj[firstKey].hasOwnProperty('date')
        || !dataObj[firstKey].hasOwnProperty('weight')) {
        return false
      }

      set(ref(db, 'logs'), dataObj)
      return true
    },
  }
})
